#include "LCD5110_Basic.h"
#include "DHT.h"
#include "LowPower.h"

#define WAKEUP 2
#define DHTPIN 4
#define DHTTYPE DHT22
#define LCD_SCE 7
#define LCD_RST 6
#define LCD_DC 5
#define LCD_DIN 11
#define LCD_SCLK 13
#define LCD_LED 9


DHT dht(DHTPIN, DHTTYPE);
LCD5110 myGLCD(LCD_SCLK, LCD_DIN, LCD_DC, LCD_RST, LCD_SCE);

extern uint8_t SmallFont[];

void showData()
{
    myGLCD.disableSleep();
    myGLCD.clrScr();
    analogWrite(LCD_LED, 200);
    myGLCD.print("Reading", CENTER, 16);
    myGLCD.print("data...", CENTER, 24);

    float t = NAN;
    float h = NAN;

    // warm up the sensor
    for (int i = 0; i < 3; i++) {
        dht.read(true);
    }

    while (isnan(t) || isnan(h)) {
        t = dht.readTemperature(false, true);  // Celsius, force refresh
        h = dht.readHumidity();
    }

    // Compute heat index in Celsius (isFahreheit = false)
    float hic = dht.computeHeatIndex(t, h, false);

    String temp_string = String(t, 2);
    String hum_string = String(h, 2);
    String heat_string = String(hic, 2);

    myGLCD.clrScr();
    analogWrite(LCD_LED, 0);
    myGLCD.print("Temperature:", LEFT, 0);
    myGLCD.print(temp_string + "~C", RIGHT, 8);
    myGLCD.print("Humidity:", LEFT, 16);
    myGLCD.print(hum_string + "%", RIGHT, 24);
    myGLCD.print("Heat index:", LEFT, 32);
    myGLCD.print(heat_string + "~C", RIGHT, 40);
    delay (3000);
    analogWrite(LCD_LED, 255);
    myGLCD.enableSleep();
}

void hdlr()
{
//nothing
}

void setup()
{
    pinMode(WAKEUP, INPUT);
    dht.begin();
    myGLCD.InitLCD();
    myGLCD.setFont(SmallFont);
    myGLCD.enableSleep();
    pinMode(LCD_LED, OUTPUT);
    analogWrite(LCD_LED, 255);
}

void loop()
{
    attachInterrupt(digitalPinToInterrupt(WAKEUP), hdlr, RISING);
    LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF);
    detachInterrupt(digitalPinToInterrupt(WAKEUP));
    showData();
}

